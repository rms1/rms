﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RMS.Web.UI.Models;
using RMS.Business.Interface;
using RMS.Web.UI.ViewModel;
using RMS.Business;
using RMS.Business.Service;
using RMS.Utility;
using RMS.Models;
using RMS.Web.UI.Authentication;
using System.Web.Security;

namespace RMS.Web.UI.Controllers
{
    public class AccountController : BaseController
    {
       UserService userservice = null;
        Email email = null;
       FormsAuthentications authService = null;

       public AccountController()
        {
          userservice=new UserService();
          email=   new Email();
          authService = new FormsAuthentications();
        }


        [AllowAnonymous]
       public ActionResult Login()
       {
           if (UserContext.IsAuthenticated)
               return Redirect("Home");

            return View();
        }
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginViewModel login)
        {
            if (ModelState.IsValid)
            {
               var user = authService.Login(login.Email, login.Password, login.RememberMe);
                if (user.Status == (int)LoginStatusEnum.InvalidPassword)
                {
                    ModelState.AddModelError("Password", "Invalid password entered");
                }
                else if (user.Status == (int)LoginStatusEnum.InvalidEmail)
                {
                    ModelState.AddModelError("Email", "This email id does not exist");

                }
                else if(user.IsAdmin==true)
                {
                  //  return Redirect("Home");
                   return RedirectToAction("index", "home", new { area = "Admin" });
                }
             }
              return View(login);
        }

        [HttpPost]
        public ActionResult ForgotPassword(Employee employeeInstance)
        {
          if (ModelState.IsValid)
            {
                var user = userservice.GetUserPassword(employeeInstance.EmailId);
                if (user.Status==(int)LoginStatusEnum.Success)
                {
                    email.SendMail(user);
                    employeeInstance.Message = "true";
                }
                else
                {
                    employeeInstance.Message = "false";
                }
            }
            return Json(employeeInstance);
        }


        
        public ActionResult LogOut()
        {
            authService.Logout();
            return RedirectToAction("Login");

        }

    }
}