﻿using RMS.Web.UI.Authentication;
using RMS.Web.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RMS.Web.UI.Controllers
{

    public class BaseController : Controller
    {
        
        private UserContext _userContext;
        protected virtual new CustomPrincipal User
        {
            get { return HttpContext.User as CustomPrincipal; }
        }

        public UserContext UserContext
        {
            get { return _userContext ?? (_userContext = new UserContext(User)); }
        }




    }
}