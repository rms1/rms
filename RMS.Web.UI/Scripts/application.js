﻿
/*
This method used for
When submit  the valid email id (for geting password) forgot password button then get the password in database(according user) then update the model window with validation.
*/
function GetUserPassword() {
    debugger;
    var emailId = $('#emailtxt').val();
    var url = "Account/ForgotPassword";
    $.ajax({
        url: url,
        type: "Post",
        datatype: "json",
        data: {
            EmailId: emailId,
        },
        success: function (response) {
            if (emailId == "" || emailId == undefined) {
                $("#valid").html("<h6 class='resultwait-text'>Please enter your email address</h1>");
            }
            else if (response.Message == "true") {
                $('#emailtxt').hide();
                $('#forgot-btn').hide();
                $("#valid").html("<h6 class='successresult-text' >Your password has been sent to your email address</h1>");
            }
            else if (response.Message == "false") {
                $("#valid").html("<h6 class='resultwait-text'>This email address does not exist</h1>");
            }
            else {
                $("#valid").html("<h6 class='resultwait-text'>Please enter a valid email address</h1>");
           }
         }
       });
}


//clear filled data in forgot model window when closing the window.
function ClearForgotFilledData() {

    document.getElementById("emailtxt").value = "";
    document.getElementById("valid").innerHTML = "";
    $('#emailtxt').show();
    $('#forgot-btn').show();

}