﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
namespace RMS.Web.UI.Authentication
{
    public class CustomPrincipal
    {
        public IIdentity Identity { get; private set; }

        public CustomPrincipal()
        {

        }
        public CustomPrincipal(string email)
        {
            Identity = new GenericIdentity(email);

        }
        public bool IsInRole(string role)
        {
            return Roles.Contains(role);
        }


        public int UserId { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string[] Roles { get; set; }

    }
}