﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RMS.Business.Service;
using RMS.Models;
using System.Web.Security;
using System.Web.Script.Serialization;

namespace RMS.Web.UI.Authentication
{
    public class FormsAuthentications
    {

        UserService _userService = null;
        private const int CookieVersion = 1;

        public FormsAuthentications()
        {
            _userService = new UserService();
        }

        // assumes user has authenticated in some way, i.e. via the confirmation
        // process. Call this method with care
        public void SetAuthenticated(User user, bool persistent = false)
        {
            if (user != null)
            {
                var cookieName = CookieName(user);
                var encryptedTicket = GenerateEncryptedTicket(user, cookieName, persistent);
                var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                HttpContext.Current.Response.Cookies.Add(authCookie);
            }
        }

        public void Reauthenticate(User user, bool isPersistent = false)
        {
            var authCookie = FormsAuthentication.GetAuthCookie(user.UserName, false);
            authCookie.Value = GenerateEncryptedTicket(user, user.UserName, isPersistent);
            HttpContext.Current.Response.Cookies.Set(authCookie);
        }
        public User Login(string username, string password, bool persistent = false)
        {
            User user = _userService.Login(username, password);
            //   status = user.Status;
            // if (user.Status == (int)LoginStatusEnum.Success)
            if (user.Status == (int)LoginStatusEnum.Success)
            {
                SetAuthenticated(user, persistent);
                //return true;
            }
            return user;
        }

        public void Logout()
        {
            FormsAuthentication.SignOut();
        }

        private string GenerateEncryptedTicket(User user, string cookieName, bool persistent)
        {
            if (user == null) return null;

            var serializeModel = new CustomPrincipalSerializeModel
            {

                Email = user.UserName

            };

            var serializer = new JavaScriptSerializer();
            var userData = serializer.Serialize(serializeModel);

            var authTicket = new FormsAuthenticationTicket(
              CookieVersion, // version
              cookieName, // name
              DateTime.UtcNow, //created
              DateTime.UtcNow.AddMinutes(240), // expires
              persistent, // persistent?
              userData // user data
              );

            return FormsAuthentication.Encrypt(authTicket);
         
        }

        private string CookieName(User user)
        {
            var name = user.UserName ?? Guid.NewGuid().ToString();
            return string.Format("__RMS_{0}", name);
        }

    }
}