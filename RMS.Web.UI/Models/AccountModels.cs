﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMS.Web.UI.Models
{
    public class AccountModels
    {
        [Required(ErrorMessage = "Please Enter email Address")]
        [Display(Name = "UserName")]
        [RegularExpression(".+@.+\\..+", ErrorMessage = "Please Enter Correct email Address")]
        public string UserName { get; set; }    
        public string Password { get; set; }
        public bool RememberMe { get; set; }
    }
}