﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RMS.Web.UI.Authentication;

namespace RMS.Web.UI.Models
{
    public class UserContext
    {
        private readonly CustomPrincipal _customPrincipal;
        public UserContext(CustomPrincipal customPrincipal)
        {
            _customPrincipal = customPrincipal;
        }
        public int UserId
        {
            get { return _customPrincipal.UserId; }
        }
        public string Email
        {
            get { return _customPrincipal.Identity.Name; }
        }
        public bool IsAuthenticated
        {
            get { return _customPrincipal != null && _customPrincipal.Identity.IsAuthenticated; }
        }
    }
}