﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RMS.Business;
using RMS.Models;
using RMS.Business.Service;

namespace RMS.Web.UI.Areas.Admin.Controllers
{
    public class VacancyController : Controller
    {
      UserService userservice = null;
          public VacancyController()
        {
            userservice = new UserService();
        }

        [HttpGet]
        public ActionResult Create()
        {
            Vacancy vacancies = new Vacancy();

            var data = userservice.GetJobLocation();
            ViewBag.locations = new SelectList(data, "Id", "locations");
            return View(vacancies);
        }

        [HttpPost]
        public ActionResult Create(Vacancy vacancies)
        {
            userservice.CreateVacancy(vacancies);
            return View(vacancies);
        }


        [HttpGet]
        public ActionResult GetJobTitle(string query)
        {
            var jobTitle = userservice.GetJobTitle();
            jobTitle = jobTitle.Where(p => p.JobTitle.StartsWith(query, StringComparison.CurrentCultureIgnoreCase)).ToList();
            return Json(jobTitle, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetJobLocation()
        {
            var data = userservice.GetJobLocation();

            return Json(data, JsonRequestBehavior.AllowGet);
          
        }



    }
}