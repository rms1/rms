﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using RMS.Models;
namespace RMS.Web.UI.ViewModel
{
    public class LoginViewModel
    {
        public IEnumerable<Employee> employee { get; set; }
        [EmailAddress(ErrorMessage = "Email id format is wrong")]
        [Required(ErrorMessage = "Please enter an  email id.")]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please enter Password.")]
        [Display(Name = "Password")]
        public string Password { get; set; }
        public bool RememberMe { get; set; }
    }
}