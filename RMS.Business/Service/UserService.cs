﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RMS.Models;
using RMS.Data;
using System.Data;
namespace RMS.Business.Service
{
    public class UserService
    {
        UserDataManager userDataManager = null;
        public UserService()
        {
            userDataManager = new UserDataManager();
        }

        public User Login(string userName, string password)
        {
            DataTable result = userDataManager.Login(userName, password);
            if (Convert.ToInt32(result.Rows[0]["login_status"]) == (int)LoginStatusEnum.InvalidEmail)
            {
                return new User { Status = Convert.ToInt32(result.Rows[0]["login_status"]) };
            }
            else if (Convert.ToInt32(result.Rows[0]["login_status"]) == (int)LoginStatusEnum.InvalidPassword)
            {
                return new User { Status = Convert.ToInt32(result.Rows[0]["login_status"]) };
            }
            else
            {
                return GetUser(result);
            }
        }


        User GetUser(DataTable dt)
        {
            return new User { UserName = dt.Rows[0]["email_id"].ToString(), Password = dt.Rows[0]["password"].ToString(),IsAdmin=Convert.ToBoolean(dt.Rows[0]["is_admin"]) };
        }



        public User GetUserPassword(string userName)
        {
            DataTable result = userDataManager.ForgotPassword(userName);
            if (Convert.ToInt32(result.Rows[0]["email_status"]) == (int)LoginStatusEnum.Success)
            {
                return GetPassword(result);
            }
            else
            {
                return new User() { Status = Convert.ToInt32(result.Rows[0]["email_status"]) };
            }
        }
        public User GetPassword(DataTable dt)
        {

            return new User { Status = Convert.ToInt32(dt.Rows[0]["email_status"]), UserName = dt.Rows[0]["email_id"].ToString(), Password = dt.Rows[0]["password"].ToString() };
        }


        public List<Vacancy> GetJobTitle()
        {
            DataTable vacancies = userDataManager.GetJobTitle();
            List<Vacancy> vacancyList = new List<Vacancy>();

            foreach (DataRow vc in vacancies.Rows)
            {
                Vacancy vacancyInstance = new Vacancy();

                vacancyInstance.Id = vc.Field<int>("id");
                vacancyInstance.JobTitle = vc.Field<string>("title");
                vacancyList.Add(vacancyInstance);
            }
            return vacancyList;
        }


        public List<Vacancy> GetJobLocation()
        {
            DataTable location = userDataManager.GetLocation();
            List<Vacancy> locationList = new List<Vacancy>();

            foreach (DataRow vc in location.Rows)
            {
                Vacancy vacancyInstance = new Vacancy();

                vacancyInstance.Id = vc.Field<int>("id");
                vacancyInstance.Locations = vc.Field<string>("name");
                locationList.Add(vacancyInstance);
            }
            return locationList;
        }





        public void CreateVacancy(Vacancy vacancy)
        {
            DataTable vacancies = userDataManager.GetVacancyDetail(vacancy.JobDescription, vacancy.Keywords,vacancy.NumberOfPositions);

        }

   
    }
}



