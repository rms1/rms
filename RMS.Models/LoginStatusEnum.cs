﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS.Models
{
    public enum LoginStatusEnum
    {
        Success = 0,
        InvalidEmail = 1,
        InvalidPassword = 2
    }
}
