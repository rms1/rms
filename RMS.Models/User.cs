﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS.Models
{
    public class User
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public int Status { get; set; }
        public Boolean IsAdmin { get; set; }
    }
}
