﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace RMS.Models
{
    public class Employee
    {
        [Required(ErrorMessage = "Please enter your email-id")]
        [Display(Name = "EmailId")]
        [EmailAddress(ErrorMessage = "Please enter valid email-id format")]
        public string EmailId { get; set; }
        public string Message { get; set; }
    }
}
