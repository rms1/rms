﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS.Models
{
    public class Vacancy
    {
        [Required(ErrorMessage = "Please enter a Job-Title.")]
        [StringLength(int.MaxValue, MinimumLength = 5)]
        [RegularExpression(@"^(?:.*[a-z]){7,}$", ErrorMessage = "Too few characters entered for Job Title,Please enter atleast 5 characters.")]
        [Display(Name = "JobTitle")]
        public string JobTitle { get; set; }


        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter a Job Description.")]
        [StringLength(int.MaxValue, MinimumLength = 5)]
        [RegularExpression(@"^(?:.*[a-z]){7,}$", ErrorMessage = "Too few characters entered for Job Description ,Please enter atleast 5 characters.")]
        [Display(Name = "JobDescription")]
        public string JobDescription { get; set; }

        public string Keywords { get; set; }
        //public string WorkExperience { get; set; }
        public int NumberOfPositions { get; set; }
         public string Locations { get; set; }

         public List<string> LocationList { get; set; }

       

    }

}