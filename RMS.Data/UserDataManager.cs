﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS.Data
{
    public class UserDataManager
    {
        SqlDataManager sqlDataManager = null;
        public UserDataManager()
        {
            sqlDataManager = new SqlDataManager();
        }
        public DataTable Login(string userName, string password)
             {
              sqlDataManager.AddParameter("email_id", DbType.String, userName);
              sqlDataManager.AddParameter("password", DbType.String, password);
              return sqlDataManager.ExecuteDataTable("validate_user");
             }

        public DataTable ForgotPassword(string userName)
             {
              sqlDataManager.AddParameter("email_id", DbType.String, userName);
              return sqlDataManager.ExecuteDataTable("getuser");
             }


        public DataTable GetJobTitle()
        {
            return sqlDataManager.ExecuteDataTable("getjob_title");
        }


        public DataTable GetVacancyDetail(string jobDescription, string keywords, int numberOfPositions)
        {
            sqlDataManager.AddParameter("job_id", DbType.Int32,2);
            sqlDataManager.AddParameter("city_id", DbType.Int32, 1);
            sqlDataManager.AddParameter("description", DbType.String, jobDescription);
            sqlDataManager.AddParameter("keywords", DbType.String, keywords);
            sqlDataManager.AddParameter("status", DbType.String, "Active");
            sqlDataManager.AddParameter("number_of_positions", DbType.Int32, numberOfPositions);

            return sqlDataManager.ExecuteDataTable("vacancy_details");
        }

        public DataTable GetLocation()
        {
            return sqlDataManager.ExecuteDataTable("get_location");
        }


    }
}
